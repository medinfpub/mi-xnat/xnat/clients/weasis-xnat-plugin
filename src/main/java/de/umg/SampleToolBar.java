/*
 * Copyright (c) 2022 Weasis Team and other contributors.
 *
 * This program and the accompanying materials are made available under the terms of the Eclipse
 * Public License 2.0 which is available at https://www.eclipse.org/legal/epl-2.0, or the Apache
 * License, Version 2.0 which is available at https://www.apache.org/licenses/LICENSE-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 */
package de.umg;

import java.awt.Component;
import java.net.MalformedURLException;
import java.net.URL;
import javax.swing.JButton;
import org.weasis.core.api.gui.util.GuiUtils;
import org.weasis.core.api.media.data.ImageElement;
import org.weasis.core.ui.util.WtoolBar;

public class SampleToolBar<E extends ImageElement> extends WtoolBar {

  protected SampleToolBar() {
    super("Sample Toolbar", 400);

    final JButton helpButton = new JButton();
    helpButton.setToolTipText("User Guide");
    helpButton.putClientProperty("JButton.buttonType", "help");
    helpButton.addActionListener(
        e -> {
          if (e.getSource() instanceof Component component) {
            URL url;
            try {
              url = new URL("https://nroduit.github.io/en/tutorials/");
              GuiUtils.openInDefaultBrowser(component, url);
            } catch (MalformedURLException e1) {
              e1.printStackTrace();
            }
          }
        });
    add(helpButton);
  }
}
